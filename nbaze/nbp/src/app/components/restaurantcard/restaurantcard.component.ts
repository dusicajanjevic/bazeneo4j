import { Component, OnInit, Input } from '@angular/core';
import { Restaurant } from 'src/app/models/restaurant.model';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-restaurantcard',
  templateUrl: './restaurantcard.component.html',
  styleUrls: ['./restaurantcard.component.scss']
})
export class RestaurantcardComponent implements OnInit {

  @Input()
  private restaurant: Restaurant;
  
  constructor(private router: Router,private dataService: DataService) { }

  ngOnInit() {
  }

  viewMenu(): void {
    this.router.navigate(['restaurant',this.restaurant.Name,this.restaurant.City]);
  }

}
