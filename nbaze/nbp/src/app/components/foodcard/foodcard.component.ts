import { Component, OnInit, Input } from '@angular/core';
import { Food } from 'src/app/models/food.model';
import { Router } from '@angular/router';
import { Picture } from 'src/app/models/picture.model';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-foodcard',
  templateUrl: './foodcard.component.html',
  styleUrls: ['./foodcard.component.scss']
})
export class FoodcardComponent implements OnInit {

  pictures:Picture[];
  pic:Picture;

  @Input()
  private food: Food;
  
  constructor(private router: Router,private dataService: DataService) { }

  ngOnInit() {
    this.loadPictures();
  }

  loadFood(): void {
    this.router.navigate(['meal', this.food.Id]);
  }

  loadPictures():void {
      this.dataService.getPicturesOfMeal(this.food.Id).subscribe(
        (data: Picture[]) => {
          this.pictures = data;
          this.pic=this.pictures[0];
    
        }
      );
      }

  
}
