import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { Food } from 'src/app/models/food.model';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.scss']
})
export class FoodComponent implements OnInit {
  myBackgroundImageUrl = 'assets/slika.png';
 

  allFood: Food[];
  allFoodF: Food[];
  Name: string;
  Name1: string;
  Type: string;
  Description:string;

  @ViewChild('form',{static: false})
  form: NgForm; 
  @ViewChild('form1',{static: false})
  form1: NgForm; 
  


  constructor(private dataService: DataService,private router: Router) { }

  ngOnInit() {
    this.loadFood();
  }

  loadFood(): void {
    this.dataService.getAllFood().subscribe(
      (data: Food[]) => {
        this.allFood = data;
      }
    );
  }

 
  find(): void {
    let Name: string = this.Name;
    this.dataService.getFoodByName(Name).subscribe(
      (data: Food[]) => {
        this.allFood = data;
           });
    //    this.loadFood();

 }
  
 find1(): void {
  let Name: string = this.Name1;
  let Description:string=this.Description;
  this.dataService.getFoodByNameAndDescription(Name,Description).subscribe(
    (data: Food[]) => {
      this.allFood=data;
         });
     // this.loadFood();

}

  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }
}


