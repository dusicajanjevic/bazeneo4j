import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Food } from 'src/app/models/food.model';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';
import { Picture } from 'src/app/models/picture.model';

@Component({
  selector: 'app-tastedcard',
  templateUrl: './tastedcard.component.html',
  styleUrls: ['./tastedcard.component.scss']
})
export class TastedcardComponent implements OnInit {
  @Input()
  private food: Food;
  @Output()
  notifyDeleteFood: EventEmitter<any> = new EventEmitter<any>();
  id:string;
  allFood:Food[];
  meal:Food;
  pictures: Picture[];
  pic: Picture;
 
  
  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.id=this.food.Id;
    this.loadFood();
    this.loadPictures();

  }

  loadFood(): void {
    this.dataService.getAllFood().subscribe(
      (data: Food[]) => {
        this.allFood = data;
      }
    );
  }

  loadPictures():void {
    this.dataService.getPicturesOfMeal(this.food.Id).subscribe(
      (data: Picture[]) => {
        this.pictures = data;
        this.pic=this.pictures[0];
  
      }
    );
    }
  deleteFood(){
    this.dataService.deleteFood(this.food.Id).subscribe(
      () => {
        this.notifyDeleteFood.emit();
        this.router.navigate(["myFood"]);
      }
    );

  }
}
