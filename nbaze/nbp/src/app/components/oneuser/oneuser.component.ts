import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { Food } from 'src/app/models/food.model';

@Component({
  selector: 'app-oneuser',
  templateUrl: './oneuser.component.html',
  styleUrls: ['./oneuser.component.scss']
})
export class OneuserComponent implements OnInit {

  user : User;
  usernameS:string
  allFood:Food[];
  recommendedFood:Food[];
  myBackgroundImageUrl = 'assets/user.png';

  


  constructor(private router: ActivatedRoute, private dataService: DataService,private route: Router) { }

  ngOnInit() {
    this.user = new User();
    this.usernameS=localStorage.getItem("Username");
    this.loadUser();
    }
  

    loadUser(): void {
    this.router.params.subscribe(
      (params: Params) => {
        let username: string = params["username"]; 
        this.dataService.getUser(username).subscribe(
          (users: User[]) => {
            this.user = users[0];
            console.log(this.user);
            this.getTastedFood(username);
            this.getRecommendedFood(this.usernameS,username);

          }
        );

      }
    );

  }
  getTastedFood(Username:string):void
  {
       
          this.dataService.getTastedFood(Username).subscribe(
            (data: Food[]) => {
              this.allFood = data;
             
            }
          );
        }

  getRecommendedFood(UsernameS:string,UsernameD:string):void
  {
       
          this.dataService.getRecommendedFood(UsernameS,UsernameD).subscribe(
            (data: Food[]) => {
              this.recommendedFood = data;
             
            }
          );
        }
  
    navigateToFood(food: Food): void {
      this.route.navigate(['meal', food.Id]);
    }

    getBackgroundImageUrl() {
      return `url(${this.myBackgroundImageUrl})`
    }
}
