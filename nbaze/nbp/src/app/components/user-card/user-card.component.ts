import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {

  
  @Input()
  private user: User;
  
  constructor(private router: Router,private dataService: DataService) { }

  ngOnInit() {
    
  }
  isLogged(): boolean {
    return this.dataService.isUserLogged();
  }

  loadUser(): void {
    this.router.navigate(['user', this.user.Username]);
  }

}
