import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { Picture } from 'src/app/models/picture.model';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  myBackgroundImageUrl = 'assets/slika.png';
  pictures: Picture[];
  //public imagesUrl;//string;
  imageObject: Array<object>;



  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getAllPictures();

  }

  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }

  
  getAllPictures(): void {
    this.dataService.getAllPictures().subscribe(
      (pictures:  Picture[]) => {
        this.pictures = pictures;
        this.setImages();

      }
    );

  }

  setImages(){
    let images: any[] = [];
    this.pictures.forEach(pic => {
      let image: any = {
        image:pic.PictureURL,
        thumbImage: pic.PictureURL,
      };
      images.push(image);
    });
    this.imageObject = images;
}

}
