import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TastedfoodComponent } from './tastedfood.component';

describe('TastedfoodComponent', () => {
  let component: TastedfoodComponent;
  let fixture: ComponentFixture<TastedfoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TastedfoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TastedfoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
