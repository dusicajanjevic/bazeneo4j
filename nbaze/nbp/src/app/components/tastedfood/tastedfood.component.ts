import { Component, OnInit } from '@angular/core';
import { Food } from 'src/app/models/food.model';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tastedfood',
  templateUrl: './tastedfood.component.html',
  styleUrls: ['./tastedfood.component.scss']
})
export class TastedfoodComponent implements OnInit {

  allFood: Food[];
  username: string;
  myBackgroundImageUrl = 'assets/mojaj.png';




  


  constructor(private dataService: DataService,private router: Router) { }

    ngOnInit() {
    this.username=localStorage.getItem("Username");
    this.loadFood();
  }

  loadFood(): void {
    this.dataService.getTastedFood(this.username).subscribe(
      (data: Food[]) => {
        this.allFood = data;
      }
    );
  }

  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }

} 
