import { Component, OnInit, EventEmitter } from '@angular/core';
import { Food } from 'src/app/models/food.model';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { User } from 'src/app/models/user.model';
import { Picture } from 'src/app/models/picture.model';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-meal',
  templateUrl: './meal.component.html',
  styleUrls: ['./meal.component.scss']
})
export class MealComponent implements OnInit {

  food: Food;
  similarlyF:Food[];
  tasters:User[];
  name:string;
  id:string;
  pictures:Picture[];
  pic:Picture;
  vote: number;
  logged:string;
  myBackgroundImageUrl = 'assets/meal.png';





  constructor(private router: ActivatedRoute, private dataService: DataService,private route: Router) { }

  ngOnInit() {
    this.logged=localStorage.getItem("Username");
    this.loadFood();
 
    }
    isLogged(): boolean {
      return this.dataService.isUserLogged();
    }

  loadFood(): void {
    this.router.params.subscribe(
      (params: Params) => {
        let Id: string = params["id"];  
        console.log(Id);
       
        this.dataService.getFoodById(Id).subscribe(
          (foodA: Food[]) => {
            console.log(foodA[0]);
            this.food = foodA[0]; 
             this.loadPictures(this.food.Id);
            this.getTasters(this.food.Id);
            this.getSimilarlyFood();
          }

        );

      }
    );
  }

  loadPictures(id:string):void {
    this.dataService.getPicturesOfMeal(id).subscribe(
      (data: Picture[]) => {
        this.pictures = data;
        this.pic=this.pictures[0];
  
      }
    );
    }

  getTasters(id:string):void {
    this.dataService.getTasters(id).subscribe(
      (data: User[]) => {
        this.tasters = data;
        console.log(this.tasters);
      }
    );
  }
  getSimilarlyFood():void {
    this.dataService.getSimilarlyFood(this.food.Name,this.food.Type,this.food.Description).subscribe(
      (data: Food[]) => {
        this.similarlyF = data;
      }
    );
  }


  sendVote(vote): void {

    this.dataService.vote(this.food.Id, this.vote,this.logged).pipe(
      debounceTime(2000) 
    ).subscribe(
      (data: any) => {
       // this.food.AverageRating = data.tip;
       ///this.notifyRateFood.emit();
       //this.route.navigate(['meal',this.food.Id]);
       this.loadFood();

    }
    )
  }

  Login()
  {
    this.route.navigate(['logIn']);
  }

  navigateToUser(user: User): void {
    this.route.navigate(['user', user.Username]);
  }

  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }
  
}
