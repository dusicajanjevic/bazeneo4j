import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-allusers',
  templateUrl: './allusers.component.html',
  styleUrls: ['./allusers.component.scss']
})
export class AllusersComponent implements OnInit {
  myBackgroundImageUrl = 'assets/users.png';
 Username:string;
 user:User;
  users: User[];
  @ViewChild('form',{static: false})
  form: NgForm; 


  constructor(private dataService: DataService,private router: Router) { }

  ngOnInit() {
    this.loadUsers();
  }

  loadUsers(): void {
    this.dataService.getAllUsers().subscribe(
      (users: User[]) => {
        this.users = users;
      }
    );
  }

  find(): void {
    let Username: string = this.Username;
    this.dataService.getUser(Username).subscribe(
      (data: User[]) => {
        this.users=data;
           });
       // this.loadFood();
  
  }

  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }
}
