import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addrestaurant',
  templateUrl: './addrestaurant.component.html',
  styleUrls: ['./addrestaurant.component.scss']
})
export class AddrestaurantComponent implements OnInit {

  name: string;
  city: string;
  myBackgroundImageUrl = 'assets/pr.png';


  @ViewChild('form',{static: false})
  form: NgForm; 
  notifyCreateRestaurant: EventEmitter<any> = new EventEmitter<any>();


  constructor(private dataService: DataService,private router: Router) { }

  ngOnInit() {


  }

  addRestaurant() {
    let name: string = this.name;
     let city: string = this.city;
   


    this.dataService.addRestaurant(name,city).subscribe(
      (success: any) => {
        if(!success){
          return;
          
        }

      }
    );
    this.notifyCreateRestaurant.emit();

    this.form.reset();

    this.router.navigate(['home']);

  }


 
  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }

}
