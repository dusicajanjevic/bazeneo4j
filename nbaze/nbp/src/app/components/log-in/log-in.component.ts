import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { User } from 'src/app/models/user.model';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
  myBackgroundImageUrl = 'assets/pr.png';
  Username: string;
  Password: string;


  @ViewChild('form',{static: false})
  form: NgForm;

  constructor(private dataService: DataService, private router: Router,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  login(): void {
    let Username: string = this.Username;
    let Password: string = this.Password;
    this.dataService.login(Username, Password).subscribe(
      (success: boolean) => {
        if(!success){
          return;
          
        }

        this.dataService.getUser(Username).subscribe(
         // (result: any) => {
          (data: User[]) => {
          console.log(data);
          let uName:string=data[0].Username;
          let pass:string=data[0].Password;
          localStorage.setItem("Username",uName);
          localStorage.setItem("Password", Password);
          let klijent: User = new User();
          klijent.Username = Username;
          klijent.Password = Password;
          this.router.navigate(["home"]);
            
          }
        );

      },
      (error) => {
        //neuspesan login
       console.log(error);
       let message = "Neuspešna prijava.Pogresili ste lozinku ili korisnicko ime";
       this.snackBar.open(message, "Zatvori", {
         duration: 2000,
       });
     }
     
    );
 }

 createProfile()
 {
       this.router.navigate(['register']);

 }
  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }

}
