import { NgModule } from '@angular/core';
import {MatSnackBarModule,MatButtonModule, MatCheckboxModule,MatTabsModule, MatMenuModule,MatInputModule,MatCardModule,MatBadgeModule,MatGridListModule,MatSidenavModule,MatExpansionModule,MatSelectModule,MatChipsModule,MatDatepickerModule,MatRadioModule,MatIconModule,MatDialogModule,MatTableModule,MatSliderModule} from '@angular/material';
@NgModule({
  imports: [MatSnackBarModule, MatButtonModule, MatCheckboxModule,MatTabsModule, MatMenuModule,MatInputModule,MatCardModule,MatBadgeModule,MatGridListModule,MatSidenavModule,MatExpansionModule,MatSelectModule,MatChipsModule,MatDatepickerModule,MatRadioModule,MatIconModule,MatDialogModule,MatTableModule,MatSliderModule],
  exports: [MatSnackBarModule, MatButtonModule, MatCheckboxModule,MatTabsModule, MatMenuModule,MatInputModule,MatCardModule,MatBadgeModule,MatGridListModule,MatSidenavModule,MatExpansionModule,MatSelectModule,MatChipsModule,MatDatepickerModule,MatRadioModule,MatIconModule,MatDialogModule,MatTableModule,MatSliderModule],
})
export class MaterialModule { }