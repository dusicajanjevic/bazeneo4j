import { Data } from './data.interface';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Food } from '../models/food.model';
import { User } from '../models/user.model';
import { Picture } from '../models/picture.model';
import { Restaurant } from '../models/restaurant.model';


@Injectable(
    {providedIn: "root"}
)
export class DataService implements Data {
  
   // private baseUrl: string = "http://localhost:3000/";
    private baseUrl: string = "https://localhost:44325/api/";
   // https://localhost:44325/


    constructor(private http: HttpClient) {

    }
    getAllFood() : Observable<Food[]> {
        let url: string = this.baseUrl + "Food/all";
        return this.http.get<Food[]>(url);
    }

    getAllUsers() : Observable<User[]>{
        let url: string = this.baseUrl + "User/all";
        return this.http.get<User[]>(url);
    }
    getAllPictures() : Observable<Picture[]>{
        let url: string = this.baseUrl + "Picture/all";
        return this.http.get<Picture[]>(url);
    }
    getAllRestaurants() : Observable<Restaurant[]>{
        let url: string = this.baseUrl + "Restaurant/all";
        return this.http.get<Restaurant[]>(url);
    }
    login(Username: string, Password: string): Observable<any> {
        let url: string = this.baseUrl + "User/LogIn";
        let payload: any = {
            Username: Username,
            Password: Password
        };
        return this.http.post<any>(url, payload, {responseType: "json"});
    }
    getUserByUsername(username: string){
        let url: string = this.baseUrl + "User/" + username;
        return this.http.get(url, {responseType: "json" });
    }

    addFood(Username:string,Name: string, Type: string,Description:string,restaurantName:string,RestaurantCity:string,pictureURL:string): Observable<any> {
        let url: string = this.baseUrl + "Food/add";
        let payload: any = {
            Username:Username,
            Name: Name,
            Type: Type,
            Description:Description,
            RestaurantName:restaurantName,
            RestaurantCity:RestaurantCity,
            PictureURL:pictureURL
        };
        return this.http.post<any>(url, payload, {responseType: "json"});
    }
    addRestaurant(Name:string,City: string): Observable<any> {
        let url: string = this.baseUrl + "Restaurant/add";
        let payload: any = {
            Name: Name,
            City: City,
        };
        return this.http.post<any>(url, payload, {responseType: "json"});
    }

    
    private convertUserToPayload(user: User): any {
        let payload: any = {
            name: user.Name,
            username: user.Username
        }

        return payload;
    }
    register(Name: string, Username: string,Password:string): Observable<any> {
        let url: string = this.baseUrl + "user/add";
        let payload: any = {
            Name: Name,
            Username:Username,
            Password:Password
        };
        return this.http.post<any>(url, payload, {responseType: "json"});
    }

    isUserLogged(): boolean {
        let username: string = localStorage.getItem("Username");
        return username != null;
    }

    getFoodByName(Name: string): Observable<any>{
        let url: string = this.baseUrl + "Food/Find/n/" + Name;
        return this.http.get<Food[]>(url);
    }
    getFoodById(Id:string): Observable<any>{
        let url: string = this.baseUrl + "Food/Find/" + Id;
        return this.http.get<Food[]>(url);
    }
    getFoodByNameAndDescription(Name:string,Description:string): Observable<any>{
        let url: string = this.baseUrl + "Food/search/" + Name + "/and/" + Description;
        return this.http.get<Food>(url);
    }

    getRestaurantByNameCity(Name:string,City:string): Observable<any>{
        let url: string = this.baseUrl + "Restaurant/" + Name + "/" + City;
        return this.http.get<Restaurant[]>(url);
    }

    getUserByUP(username: string,password:string): Observable<any>{
        let url: string = this.baseUrl + "User/search/" + username + "/and/" + password;
        return this.http.get<User[]>(url);
    }
    getUser(Username:string): Observable<any>{
        let url: string = this.baseUrl + "User/" + Username ;
        return this.http.get<User[]>(url);

    }
    getRestaurantByCity(City: string): Observable<any>{
        let url: string = this.baseUrl + "Restaurant/getByCity/" + City ;
        return this.http.get<Restaurant[]>(url);

    }

    getTastedFood(Username:string) :Observable<any>{
        let url: string = this.baseUrl + "User/tastedFood/" + Username ;
        return this.http.get<Food[]>(url);
    }
    getRecommendedFood(UsernameS,UsernameD):Observable<any>{
        let url: string = this.baseUrl + "User/recomendedFood/" + UsernameS + "/and/" + UsernameD ;
        return this.http.get<Food[]>(url);
    }
    addPicture(FoodName: string, PictureURL: string): Observable<any> {
        let url: string = this.baseUrl + "Picture/add";
        let payload: any = {
            FoodName: FoodName,
            PictureURL: PictureURL,
        };
        return this.http.post<any>(url, payload, {responseType: "json"});
    }
    link(FoodId: string, PictureURL: string): Observable<any> {
        let url: string = this.baseUrl + "Picture/storeImageUrl";
        let payload: any = {
            FoodId: FoodId,
            PictureURL: PictureURL,
        };
        return this.http.post<any>(url, payload, {responseType: "json"});
    }
    getTasters(Id:string) :Observable<any>{
        let url: string = this.baseUrl + "Food/tasters/" + Id;
        return this.http.get<User[]>(url);
    }
    getPicturesOfMeal(Id:string):Observable<any>{
        let url: string = this.baseUrl + "Food/getPictures/" + Id;
        return this.http.get<Picture[]>(url);
    }
    vote(id:string,rate:number,username:string) :Observable<any>{
        let url: string = this.baseUrl + "Food/vote";
        let payload: any = {
        };
       
        payload.Id = id;
        payload.Rating=rate;
        payload.Username=username;
          return this.http.post<number>(url,payload);
        
    }  

    deleteFood(id:string):Observable<any> {
        let url: string = this.baseUrl + "Food/" + id;
            return this.http.delete(url);
        }

    getSimilarlyFood(name:string,type:string,desc:string): Observable<any>{
        let url: string = this.baseUrl + "Food/similarly";
        let payload: any = {
        };
        payload.Name = name;
        payload.Type=type;
        payload.Description=desc;
          return this.http.post<number>(url,payload);
    }
    getMenu(Name:string,City:string): Observable<any>{
        let url: string = this.baseUrl + "Restaurant/menu/" + Name + "/and/" + City;
       
          return this.http.get<Food[]>(url);
    }
}
